import { animate, state, style, transition, trigger } from '@angular/animations';

export const slideInDownAnimation =
  trigger('routeAnimation', [
    state('*',
      style({
        opacity: 1,
      })
    ),
    transition(':enter', [
      style({
        opacity: 0,
        transform: 'translateX(-10px)'
      }),
      animate('0.5s ease-in-out')
    ]),
    transition(':leave', [
      animate('0.5s ease-in-out', style({
        opacity: 0,
        transform: 'translateX(10px)'
      }))
    ])    
  ]);