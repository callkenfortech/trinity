import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInDownAnimation } from "./../animations";

@Component({
  selector: 'app-staff',
  animations: [slideInDownAnimation],
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display')   display = 'block';
  @HostBinding('style.position')  position = 'absolute';
  @HostBinding('style.width')  width = '100%';
  @HostBinding('style.height')  height = '100%';

  constructor() { }

  ngOnInit() {
  }

}
