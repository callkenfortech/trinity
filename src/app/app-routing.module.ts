import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { WhatWeBelieveComponent } from './what-we-believe/what-we-believe.component';
import { StaffComponent } from './staff/staff.component';
import { OutreachComponent } from './outreach/outreach.component';
import { PodcastsComponent } from './podcasts/podcasts.component';
import { TithesComponent } from './tithes/tithes.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'what-we-believe', component: WhatWeBelieveComponent },
  { path: 'staff', component: StaffComponent },
  { path: 'outreach', component: OutreachComponent },
  { path: 'podcasts', component: PodcastsComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'home/giving', component: TithesComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
