import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInDownAnimation } from "./../animations";

@Component({
  selector: 'app-what-we-believe',
  animations: [slideInDownAnimation],
  templateUrl: './what-we-believe.component.html',
  styleUrls: ['./what-we-believe.component.css']
})
export class WhatWeBelieveComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display')   display = 'block';
  @HostBinding('style.position')  position = 'absolute';
  @HostBinding('style.width')  width = '100%';
  @HostBinding('style.height')  height = '100%';
  constructor() { }

  ngOnInit() {
  }

}
