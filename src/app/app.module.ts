import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { WhatWeBelieveComponent } from './what-we-believe/what-we-believe.component';
import { StaffComponent } from './staff/staff.component';
import { OutreachComponent } from './outreach/outreach.component';
import { PodcastsComponent } from './podcasts/podcasts.component';
import { TithesComponent } from './tithes/tithes.component';
import { HttpClientModule } from '@angular/common/http';
import { AdminComponent } from './admin/admin.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WhatWeBelieveComponent,
    StaffComponent,
    OutreachComponent,
    PodcastsComponent,
    TithesComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
