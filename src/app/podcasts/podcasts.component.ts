import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInDownAnimation } from "./../animations";

@Component({
  selector: 'app-podcasts',
  animations: [slideInDownAnimation],
  templateUrl: './podcasts.component.html',
  styleUrls: ['./podcasts.component.css']
})
export class PodcastsComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display')   display = 'block';
  @HostBinding('style.position')  position = 'absolute';
  @HostBinding('style.width')  width = '100%';
  @HostBinding('style.height')  height = '100%';
  
  constructor() { }

  ngOnInit() {
  }

}
