import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInDownAnimation } from "./../animations";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  animations: [slideInDownAnimation],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display')   display = 'block';
  @HostBinding('style.position')  position = 'absolute';
  @HostBinding('style.width')  width = '100%';
  @HostBinding('style.height')  height = '100%';
  events
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get('http://178.128.168.109:3000/api/events').subscribe(
      res => {
        this.events = res;
      }, 
      err => {
        console.log(err.error);
      }
    )
  }

  

  

}
