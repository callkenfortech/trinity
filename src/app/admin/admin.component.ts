import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  events
  newEvent = {
    title: '',
    body: ''
  }
  seeAdmin: boolean = false

  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.getEvents();
  }

  letMeAdmin(pass) {
    if(pass == 'hasky') {
      this.seeAdmin = true;
    } else {
      alert('password incorrect');
    }
  }

  getEvents() {
    this.http.get('http://178.128.168.109:3000/api/events').subscribe(
      res => {
        this.events = res;
        this.newEvent.title = '';
        this.newEvent.body = '';
      }, 
      err => {
        console.log(err.error);
      }
    )
  }

  saveEvent() {
    this.http.post('http://178.128.168.109:3000/api/events', this.newEvent).subscribe(
      res => {
        this.getEvents();
      }, 
      err => {
        console.log(err.error);
      }
    )
  }

  deleteEvent(event) {
    this.http.get('http://178.128.168.109:3000/api/events/'+event).subscribe(
      res => {
        this.getEvents();
      }, 
      err => {
        console.log(err.error);
      }
    )
  }


}
