import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInDownAnimation } from "./../animations";

@Component({
  selector: 'app-outreach',
  animations: [slideInDownAnimation],
  templateUrl: './outreach.component.html',
  styleUrls: ['./outreach.component.css']
})
export class OutreachComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display')   display = 'block';
  @HostBinding('style.position')  position = 'absolute';
  @HostBinding('style.width')  width = '100%';
  @HostBinding('style.height')  height = '100%';

  constructor() { }

  ngOnInit() {
  }

  
}
